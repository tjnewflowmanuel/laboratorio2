package com.edilberto.sesion02tarea03app
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_destino.*
import kotlinx.android.synthetic.main.activity_main.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        val bundleRecepcion :Bundle? = intent.extras
        bundleRecepcion?.let {LibredeNull ->
             val nombre = bundleRecepcion?.getString("KEY NOMBRES","No ha ingresado su nombre")
             val edad = bundleRecepcion?.getString("KEY EDAD","No ha ingresado su edad")
             val tipo = bundleRecepcion?.getString("KEY TIPO","No ha seleccionado el tipo de mascota")

            tvnombreresultaado.text=nombre
            tvedadresultado.text=edad
            tvtiporesultado.text=tipo

            if (tipo =="PERRO"){
                tvtiporesultado.text="PERRO"
                img2.setImageResource(R.drawable.perro)
            }
            else if (tipo=="GATO") {
                tvtiporesultado.text="GATO"
                img2.setImageResource(R.drawable.gato)
            }
            else if (tipo=="CONEJO") {
                tvtiporesultado.text="CONEJO"
                img2.setImageResource(R.drawable.conejo)
            }
        }
        }
    }
