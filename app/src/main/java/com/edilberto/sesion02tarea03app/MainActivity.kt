package com.edilberto.sesion02tarea03app
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        radioGroup2.setOnCheckedChangeListener{ group, checkedId ->
            if (checkedId==R.id.rdtgato){
                Toast.makeText(this,rdtgato.text.toString(),Toast.LENGTH_SHORT).show()
                imageView.setImageResource(R.drawable.gato)
            }

            if (checkedId==R.id.rdtperro){
                Toast.makeText(this,rdtperro.text.toString(),Toast.LENGTH_SHORT).show()
                imageView.setImageResource(R.drawable.perro)}

            if (checkedId==R.id.rdtconejo){
                Toast.makeText(this,rdtconejo.text.toString(),Toast.LENGTH_SHORT).show()
                imageView.setImageResource(R.drawable.conejo)}
        }
        btnenviar.setOnClickListener {
            val nombre=edtnombre.text.toString()
            val edad=edtedad.text.toString()

            if (nombre.isEmpty()||edad.isEmpty()){
                Toast.makeText(this,"Ingrese el dato faltante",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!check1.isChecked && !check2.isChecked && !check3.isChecked && !check4.isChecked && !check5.isChecked){
                Toast.makeText(this,"Marque alguna opcion",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            var tipo=""
            if (rdtgato.isChecked){tipo="GATO"}
            if (rdtperro.isChecked){tipo="PERRO"}
            if (rdtconejo.isChecked){tipo="CONEJO"}

            val bundle = Bundle().apply {
                putString("KEY NOMBRES",nombre)
                putString("KEY EDAD",edad)
                putString("KEY TIPO",tipo)  }


            val intent = Intent(this,DestinoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}